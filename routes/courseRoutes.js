const express = require('express');
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth.js");

router.post("/", auth.verify, courseControllers.addCourse);

router.get("/allActiveCourses", courseControllers.getAllActive) ;

router.get("/getAllCourses", auth.verify, courseControllers.getAllCourses);

router.get("/:courseId", courseControllers.getCourse);

router.put("/update/:courseId", auth.verify, courseControllers.updateCourse);

router.patch("/:courseId/archive", auth.verify, courseControllers.archiveCourse);

module.exports = router;