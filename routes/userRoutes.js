const express = require('express');
const router = express.Router();
const auth = require("../auth")

const userController = require('../controllers/userControllers');

router.post("/checkEmail", userController.checkEmailExists);

router.post("/register",userController.checkEmailExists, userController.registerUser);

router.post("/login", userController.loginUser);

router.post("/details",auth.verify ,userController.getUserDetails);

router.get("/profile", userController.profileDetails);

router.patch("/updateRole/:userId", auth.verify, userController.updateRole);

router.post("/enroll/:courseId", auth.verify, userController.enroll);

module.exports = router;