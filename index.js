const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes')

const app = express();
app.use(cors());
app.use(express());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/users', userRoutes);
app.use('/courses', courseRoutes);


mongoose.connect("mongodb+srv://admin:admin@zuittbatch243-tiagan.mtb2jt6.mongodb.net/bookingAPI?retryWrites=true&w=majority",
				{ 
				  useNewUrlParser:true,
				  useUnifiedTopology: true
				}
			);

mongoose.connection.on("error", console.error.bind(console, "connection error"));

mongoose.connection.once('open', () => console.log("Connected to MongoDB"));

app.listen(process.env.PORT || 4000, () =>{
	console.log(`API running on port ${process.env.PORT||4000}`)
})