const jwt = require('jsonwebtoken');

//used for encrypting data
const secret = "CourseBookingAPI"

// [Section] JSON Web token

//Token creation
/*
	Analogy:
		pack the gift provided with a lock, which can only be opened using the secret  code as the key.
*/

module.exports.createAccessToken = (user) =>{
	const data = {
		id: user.id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	/*
		Syntax:
			jwt.sign(payload, secretOrPrivateKey, [callbackfunction])
	*/

	return jwt.sign(data, secret, {});
}

// Token Verification

module.exports.verify = (req, res , next) => {
	
	let token = req.headers.authorization;

	if(token !== undefined){

		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return res.send("Invalid Token");
			}else{
				next();
			}
		});
	}else{
		res.send("Authentication failed! No token provided");
	}
}

// Token decryption 


module.exports.decode = (token) =>{
	if(token === undefined){
		return null;
	}else{
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return null;
			}else{
				// decode method is used to obtain the information from the JWT.
				/*
					Syntax: jwt.decode(token, [options])
				*/
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
}