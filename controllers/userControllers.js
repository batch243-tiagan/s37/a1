const User = require('../models/Users');
const Course = require('../models/Courses');
const bcrypt = require('bcrypt');

const auth = require("../auth");

module.exports.checkEmailExists = (req, res, next) => {
	return User.find({email : req.body.email}).then(result =>{
		if(result.length > 0){
			return res.send(`${req.body.email} already exists`);
		}else{
			next();
		}
	}).catch(err =>{
		console.log(err);
		return res.send(`Error check console`);
	});
}

module.exports.registerUser = (req, res) => {
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	});

	return newUser.save().then(user => {
		console.log(user);
		res.send(`User registered`);
	}).catch(err =>{
		console.log(err);
		res.send(`User not registered`);
	});
}

module.exports.loginUser = (req, res) =>{
	return User.findOne({email: req.body.email}).then(result =>{
		if(result == null){
			res.send(`Your email: ${req.body.email} is not registered`);
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			if(isPasswordCorrect){
				let token = auth.createAccessToken(result);
				return res.send({accessToken: auth.createAccessToken(result)});
			}else{
				return res.send(`Incorrect password,`);
			}
		}
	}).catch(err => {
		console.log(err);
		return res.send(`Error check console`);
	});
}

module.exports.getUserDetails = (req, res) =>{
	return User.findById(req.body.id).then(result =>{
			result.password = "";
			res.send(result);
	}).catch(err =>{
		console.log(err);
		return res.send(`Error check console`);
	});
}

module.exports.profileDetails = (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	return User.findById(userData.id).then(result =>{
		console.log(result);
		result.password = "No Looky Looky";
		return res.send(result);
	}).catch(err =>{
		return res.send(err);
	})
}

module.exports.updateRole = (req, res) =>{
	let idToBeUpdated = req.params.userId;
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	if(userData.isAdmin){
		return User.findById(idToBeUpdated).then(result =>{
			return User.findByIdAndUpdate(idToBeUpdated, {isAdmin: !result.isAdmin}, {new: true})
			.then(document => {
				document.password = "Confidential";
				res.send(document)
			}).catch(err => res.send(err));
		}).catch(err =>res.send(err));
	}else{
		return res.send("User does not have access");
	}
}

module.exports.enroll = async (req, res) =>{
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	if(!userData.isAdmin){
		let data = {
			courseId: req.params.courseId,
			userId: userData.id
		}

		let isCourseUpdated = await Course.findById(data.courseId)
		.then(result =>{
			result.enrollees.push({
				userId: data.userId
			});
			result.slot -= 1;
			return result.save()
			.then(success => true)
			.catch(err => false);
		}).catch(err => res.send(false));

		let isUserUpdated = await User.findById(data.userId)
		.then(result=>{
			result.enrollments.push({
				courseId: data.courseId
			});
			return result.save().then(success => true)
			.catch(err => false);
		}).catch(err => res.send(false));

		(isUserUpdated && isCourseUpdated) ? res.send("You are now enrolled") : res.send("We encountered an error in your enrollment please try again");

	}else{
		return res.send("User has admin privilages, cannot access regular user functions")
	}
}