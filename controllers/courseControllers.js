const Course = require('../models/Courses');
const auth = require("../auth.js");

module.exports.addCourse = (req, res) =>{
		if(isAdmin){		
				let newCourse = new Course({
					name: req.body.name,
					description: req.body.description,
					price: req.body.price,
					slot: req.body.slot
				});

				newCourse.save().then(result =>{
					console.log(result);
					res.send(true);	
				}).catch(err =>{
					console.log(err);
					res.send(false);
				})
		}else{
			res.send("User is not admin");
		}
}

module.exports.getAllActive = (req, res) =>{
	return Course.find({isActive: true}).then(result => {
		res.send(result);
	}).catch(err =>{
		res.send("Error check console");
		console.log(err);
	});
}

module.exports.getCourse = (req, res) =>{
	return Course.findById(req.params.courseId)
	.then(result =>{
		res.send(result);
	}).catch(err =>{
		res.send("Error check console");
		console.log(err);
	})
}

module.exports.updateCourse = (req, res) =>{
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		slot: req.body.slots
	}

	if(userData.isAdmin){
		return Course.findByIdAndUpdate(req.params.courseId, updatedCourse, {new: true})
		.then(result =>{
			res.send(result);
		}).catch(err =>{
			res.send("Error check console");
			console.log(err);
		});
	}else{
		return res.send("User does not have access");
	}
}

module.exports.archiveCourse = (req, res) =>{
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	let courseActiveStatus = {
		isActive: req.body.isActive
	}

		if(userData.isAdmin){
			return Course.findByIdAndUpdate(req.params.courseId, courseActiveStatus, {new: true})
			.then(result =>{
				res.send(true);
			}).catch(err =>{
				res.send(false);
				console.log(err);
			});
		}else{
			return res.send("User does not have access");
		}
}

module.exports.getAllCourses = (req, res) =>{
	const token = req.headers.authorization;
	const userData = auth.decode(token);
	if(userData.isAdmin){
		return Course.find().then(result =>res.send(result))
			.catch(err =>{
			res.send("Error check console");
			response.send(err);
		});
	}else{
		return response.send("User does not have access");
	}
}